require('../test-setup');
const calcValor = require('../public/assets/js/calculadora'); 

describe('Calculadora', function () {
  beforeEach(function () {
    // Crie um contêiner simulado para o DOM
    const container = document.createElement('div');
    container.innerHTML = `
      <input type="text" id="vlr" value="100">
      <input type="text" id="desc" value="10">
      <p id="resultado"></p>
      <p id="error-message"></p>
    `;
    document.body.appendChild(container);
  });

  it('deve calcular o valor do desconto corretamente para valor e desconto válidos', function () {
    const vlrInput = document.getElementById('vlr');
    const descInput = document.getElementById('desc');
    const resultado = document.getElementById('resultado');

    calcValor(vlrInput, descInput, resultado);

    expect(resultado.textContent).toBe('R$ 90.00');
  });

  it('deve exibir mensagem de erro para valores inválidos', function () {
    const vlrInput = document.getElementById('vlr');
    const descInput = document.getElementById('desc');
    const errorMessage = document.getElementById('error-message');
    const resultado = document.getElementById('resultado');

    calcValor(vlrInput, descInput, resultado);

    expect(errorMessage.textContent).toBe('Valor inválido. Por favor, digite números válidos.');
    expect(resultado.textContent).toBe('');
  });
});
