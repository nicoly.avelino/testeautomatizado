// Importe o arquivo de configuração do ambiente do DOM simulado
require('./public/assets/js/calculadora');

const { JSDOM } = require('jsdom');

const { window } = new JSDOM('<!doctype html><html><body></body></html>');
global.document = window.document;
global.window = window;

// Importe a função calcValor (ajuste o caminho conforme necessário)
const calcValor = require('./public/assets/js/calculadora');

// Torne a função calcValor global para que os testes possam acessá-la
global.calcValor = calcValor;
