# Desenvolvimento de Site, uso do GitLab e Teste Automatizado

## Membros:
Carolina Silva
Nicoly Avelino

## Desenvolvimento – Site para calcular desconto
Este é um projeto de um site que permite aos usuários inserir o preço de um produto e um percentual de desconto. A aplicação tem a função de calcular o valor com desconto e exibir o resultado em uma label.

![Tela](https://gitlab.com/nicoly.avelino/testeautomatizado/-/design_management/designs/1639086/4256422bfea51d8fac5c0670c77f0430556ba0c2/raw_image)

## O Conceito de Pipeline no GitLab
No GitLab, o termo "pipeline" refere-se a uma prática de automação e integração contínua (CI/CD) que ajuda a organizar e automatizar o processo de desenvolvimento de software. Uma pipeline no GitLab é um conjunto de etapas ou jobs que são executados sequencial ou paralelamente para construir, testar e implantar um aplicativo ou projeto.

A pipeline começa quando há uma alteração em um repositório GitLab, como um novo código sendo enviado. O GitLab então inicia a execução da pipeline, que inclui estágios como compilação, testes automatizados, análise de código, construção de artefatos, implantação em ambientes de teste ou produção, entre outros.

Cada etapa ou job na pipeline é configurada para executar uma tarefa específica, e os resultados de cada job afetam o fluxo da pipeline. Se um job falhar, a pipeline pode ser marcada como falha e parar a execução ou continuar com outras etapas, dependendo da configuração.

A automação de pipelines no GitLab ajuda as equipes a garantir que as mudanças de código sejam testadas de forma consistente, integradas e implantadas de maneira eficiente, permitindo uma entrega de software mais rápida e confiável. Além disso, fornece visibilidade do progresso e do estado das implantações, facilitando a detecção e correção de problemas de forma ágil.


## Criação de um Pipeline em três estágios: Construção, Teste e Implantação.

A criação de um pipeline em três estágios - Construção, Teste e Implantação - é uma abordagem organizada que ajuda a otimizar o desenvolvimento de software. 

```yaml

image: python:latest

stages:
  - Construcao
  - Teste
  - Implantação

Construcao:
  stage: Construcao
  script:
    - echo "Estágio de construção..."

Teste:
  stage: Teste
  script:
    - echo "Estágio de teste..."

Implantação:
  stage: Implantação
  script:
    - echo "Estágio de implantação..."

pages:
  stage: Implantação  
  script:
    - echo "Publicando páginas..."
  artifacts:
    paths:
      - public
```
### Visualização dos testes

![3estagios](https://gitlab.com/nicoly.avelino/testeautomatizado/-/design_management/designs/1639087/4256422bfea51d8fac5c0670c77f0430556ba0c2/raw_image)

##Teste Automatizado

O teste automatizado é uma parte fundamental do processo de integração contínua (CI) e desempenha um papel crucial na verificação da precisão do código do nosso projeto. Neste caso, utilizamos a estrutura Jasmine para criar um teste automatizado que verifica os cálculos de desconto em um site. 

#### Implemente um teste automatizado que verifique a precisão dos cálculos de desconto em seu site.

Configuração do Ambiente de Teste

Para implementar esse teste automatizado, configuramos um ambiente de teste usando o framework de testes Jasmine. O Jasmine é uma ferramenta amplamente usada para testes unitários em JavaScript. Ele permite criar e executar testes de forma eficiente.

```
var calculadora = require('../../public/assets/js/calculadora');
describe('Calculo de desconto', function () {
  it('deve calcular o desconto corretamente', function () {
    var vlrLiquido = 100;  // Valor líquido
    var desc = 20;  // Desconto de 20%

    var resultado = calculadora.calcValor(vlrLiquido, desc);

    var valorEsperado = 80;  // Resultado esperado após o desconto de 20%

    expect(resultado).toEqual(valorEsperado);
  });
});
  
```

### Visualização dos testes

![teste](https://gitlab.com/nicoly.avelino/testeautomatizado/-/design_management/designs/1639088/4256422bfea51d8fac5c0670c77f0430556ba0c2/raw_image)

#### Identificando Falhas
Intencionalmente, fizemos uma alteração no código do cálculo de desconto, causando uma falha proposital. O teste automatizado no Pipeline deve identificar essa falha. A evidência da falha será visível nos resultados do Pipeline, indicando que o teste não foi aprovado.

![erro](https://gitlab.com/nicoly.avelino/testeautomatizado/-/design_management/designs/1639092/425e1bd851118bda75dd69a5f1645e3703e83400/raw_image)
![erro](https://gitlab.com/nicoly.avelino/testeautomatizado/-/design_management/designs/1639091/425e1bd851118bda75dd69a5f1645e3703e83400/raw_image)
