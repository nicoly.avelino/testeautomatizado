function calcValor() {
  document.getElementById("total").value = "0";
  var mostrar = document.getElementById("resultado");
  var errorMessage = document.getElementById("error-message");

  var vlrLiquido = parseFloat(document.getElementById("vlr").value);
  var desc = parseFloat(document.getElementById("desc").value);

  if (isNaN(vlrLiquido) || isNaN(desc)) {
    errorMessage.innerText = "Valor inválido. Por favor, digite números válidos.";
    mostrar.innerHTML = "";
  } else {
    errorMessage.innerText = "";

    var desconto = parseFloat((vlrLiquido * desc) / 100);
    var total = parseFloat(vlrLiquido) - parseFloat(desconto);
    var vlrFinal = "R$ " + total.toFixed(2);

    mostrar.innerHTML = vlrFinal;
  }
}